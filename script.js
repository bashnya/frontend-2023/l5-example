// GET-запрос по HTTP

fetch('https://jsonplaceholder.typicode.com/posts/1')
    .then(response => response.json())
    .then(data => console.log(data));

// -------------------
// POST-запрос по HTTP

const data = { username: 'example' };

fetch('https://example.com/api/user', {
    method: 'POST',
    headers: {
    'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
})
.then(response => response.json())
.then(data => console.log(data));

// -------------------
// Создание, открытие, прослушивание и закрытие сокета 

const socket = new WebSocket('wss://example.com/ws');

socket.addEventListener('open', function (event) {
  socket.send('Hello Server!');
});

socket.addEventListener('message', function (event) {
  console.log('Server says:', event.data);
});


socket.addEventListener('close', function (event) {
  console.log('Connection closed');
});
